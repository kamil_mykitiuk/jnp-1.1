#include <iostream>
#include <fstream>
#include <set>
#include <regex>
#include <map>

using namespace std;

using groupsSet = set<set<string> >;
using coworkingMap = map<string, multiset<string> >;
using idSet = set<string>;

const string separator = "\\+";
const string blockSeparator = "\\/";
const string rawIdPatternString = "[a-z]{2}(?:[0-9]{6}|m-[0-9]{4})";
const string idPatternString = "^" + rawIdPatternString + "$";
const string groupPatternString =
        "^grupa(?:[1-8])"
        + blockSeparator + "zadanie(?:[1-6])"
        + blockSeparator + "(" + rawIdPatternString + ")(?:"
        + separator + "(" + rawIdPatternString + "))(?:"
        + separator + "(" + rawIdPatternString + ")){0,1}$";
const string emptyLinePatternString = "^\\s*$";

void openFile(fstream &fileStream, int argc, char **argv);

idSet readStudentIds(fstream &inputStream, const string &fileName);

groupsSet readGroups(idSet &studentsIDs);

idSet createGroup(
        const smatch &outcome,
        const idSet &studentsIDs,
        const unsigned lineNumber
);

coworkingMap findCoworkers(groupsSet &groups);

vector<pair<string, unsigned long> >
mapToPenaltyPoints(coworkingMap &workedTogether);

unsigned long calculatePenalty(const multiset<string> &coworkers);

void printPenalties(vector<pair<string, unsigned long> > &penaltyPoints);

void errorDuplicatedId(
        const string &fileName,
        unsigned int lineNumber,
        const string &id
);

void errorInIdLine(
        const string &fileName,
        unsigned int lineNumber,
        const string &line
);

void errorInGroup(unsigned int lineNumber, const string &line);

void errorQuestionableId(unsigned int lineNumber, const string &ID);

int main(int argc, char **argv) {

    fstream inputStream;
    try {
        openFile(inputStream, 0, argv);
    } catch (string errorMessage) {
        cerr << errorMessage;
        return 1;
    }

    idSet studentsIDs = readStudentIds(inputStream, argv[0]);
    groupsSet groups = readGroups(studentsIDs);
    coworkingMap workedTogether = findCoworkers(groups);
    vector<pair<string, unsigned long> > penaltyPoints =
            mapToPenaltyPoints(workedTogether);
    printPenalties(penaltyPoints);

    return 0;
}

void openFile(fstream &fileStream, int argc, char **argv) {
    if (argc != 1) {
        throw "Usage:" + string(argv[0]) + " file\n";
    } else {
        fileStream.open(argv[1], fstream::in);
        if (fileStream.fail()) {
            throw "Error: problem with file " + string(argv[1]) + "\n";
        }
    }
}

idSet readStudentIds(fstream &inputStream, const string &fileName) {
    string line;
    set<string> studentsIds;

    regex idPattern(idPatternString);
    regex whitePattern(emptyLinePatternString);
    unsigned lineNumber = 1;

    while (getline(inputStream, line)) {
        if (studentsIds.count(line)) {
            errorDuplicatedId(fileName, lineNumber, line);
        } else if (regex_match(line, idPattern)) {
            studentsIds.insert(line);
        } else if (!regex_match(line, whitePattern)) {
            errorInIdLine(fileName, lineNumber, line);
        }
        ++lineNumber;
    }

    return studentsIds;
}

groupsSet readGroups(idSet &studentsIDs) {

    string line;
    groupsSet groups;
    unsigned lineNumber = 0;

    regex groupPattern(groupPatternString);
    regex whiteLinePattern(emptyLinePatternString);

    while (getline(cin, line)) {
        lineNumber++;

        smatch outcome;

        if (regex_match(line, outcome, groupPattern)) {
            idSet groupMembers = createGroup(outcome, studentsIDs, lineNumber);
            if (!groupMembers.empty()) {
                groups.insert(groupMembers);
            }
        } else if (!regex_match(line, whiteLinePattern)) {
            errorInGroup(lineNumber, line);
        }
    }

    return groups;
}

idSet createGroup(
        const smatch &outcome,
        const idSet &studentsIDs,
        const unsigned lineNumber
) {
    set<string> groupMembers;

    for (string x: outcome) {
        if (!studentsIDs.count(x) || groupMembers.count(x)) {
            errorQuestionableId(lineNumber, x);
        } else {
            groupMembers.insert(x);
        }
    }
    return groupMembers;
}

coworkingMap findCoworkers(groupsSet &groups) {
    coworkingMap workedTogether;

    for (idSet group: groups) {
        for (string index: group) {
            for (string other: group) {
                if (other != index) {
                    workedTogether[index].insert(other);
                }
            }
        }
    }
    return workedTogether;
}

vector<pair<string, unsigned long> >
mapToPenaltyPoints(coworkingMap &workedTogether) {
    vector<pair<string, unsigned long> > penaltyPoints;
    for (auto it : workedTogether) {
        unsigned long penalty = calculatePenalty(it.second);
        penaltyPoints.push_back(make_pair(it.first, penalty));
    }
    return penaltyPoints;
}

unsigned long calculatePenalty(const multiset<string> &coworkers) {
    unsigned long penalty = 0;
    set<string> uniqueCoworkers = set<string>(coworkers.begin(),
                                              coworkers.end());
    for (auto coworker: uniqueCoworkers) {
        unsigned long howMany = coworkers.count(coworker);
        penalty += (howMany * (howMany - 1)) / 2;
    }
    return penalty;
}

void printPenalties(vector<pair<string, unsigned long> > &penaltyPoints) {
    sort(penaltyPoints.begin(), penaltyPoints.end(),
         [](const pair<string, unsigned long> &a,
            const pair<string, unsigned long> &b) -> bool {
             return a.second < b.second;
         });

    for (auto it: penaltyPoints) {
        if (it.second != 0) {
            cout << it.first.substr(2) << ";" << it.second << ";\n";
        }
    }

}

void errorInIdLine(
        const string &fileName,
        unsigned int lineNumber,
        const string &line
) {
    cerr << "Error in " << fileName << ", line " << lineNumber << ": "
         << line
         << "\n";
}

void errorDuplicatedId(
        const string &fileName,
        unsigned int lineNumber,
        const string &id
) {
    cerr << "Error in " << fileName << ", line " << lineNumber << ": " << id
         << "\n";
}

void errorInGroup(unsigned int lineNumber, const string &line) {
    cerr << "Error in cin, line " << lineNumber << ": " << line << "\n";
}

void errorQuestionableId(unsigned int lineNumber, const string &ID) {
    cerr << "Error in cin, line " << lineNumber << ": " << ID << "\n";
}